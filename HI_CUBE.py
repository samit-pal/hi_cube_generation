import numpy as np
from pyfiglet import Figlet as f
from astropy import units as u 
from astropy import constants as const
from astropy.cosmology import Planck18, default_cosmology
x=f(font='slant')
print(x.renderText('Observational Parameters'))

def delta_nu(redshift,delx,cosmo=None):
    if cosmo is None:
        cosmo=default_cosmology.get()
    return (((new0*cosmo.H0*cosmo.efunc(redshift)*delx)/(boxsize*const.c*(redshift+1)**2)).to(u.Hz))

def calc_freq(red_shift):
    return new0/(1 + red_shift)


# PARAMETERS--------------------------------

new0 = 1420405751.77*u.Hz # rest frame HI in Hz
baseline=2000*u.m # 2km
disc_dia= 40*u.m # 40m
n= 6 # no of simpling
boxsize=256 
delx=143.36*u.Mpc
grid_resolution=delx/boxsize

z=float(input('Enter redshift: '))
## Observation parameters ----------------------------------------
Dc=Planck18.comoving_transverse_distance(z)
del_theta_rad=(delx/(boxsize*Dc))*u.rad  # Angular resolution in rad
del_theta_deg =del_theta_rad.to('deg')  # angular resolution in degree
del_theta_arcsec=del_theta_rad.to(u.arcsec)
del_nu=delta_nu(z,delx,cosmo=None)
fcentre = new0/(1+z)  # Central frequency
bandwidth=del_nu*boxsize
fstart = fcentre - (bandwidth/2)
fend = fcentre + (bandwidth/2)
f_cen=round(fcentre.value,2)
f_start=round(fstart.value,2)
channel_width=round(del_nu.value,2)  # Challenwidth
resolution=((const.c/(fcentre*baseline))*u.rad) 
fov=((const.c/(fcentre*disc_dia))*u.rad)
cell_size=(resolution/n).to(u.arcsec)
cell=round(cell_size.value,1)
imagesize=int(del_theta_arcsec.value*boxsize/cell)

print("\n\n---------------- OBservational Parameters ------------------------------\n\n")
print("\ncentral frequency: ",f_cen)
print("\nChallen width: ",channel_width)
print("\nCell size: ",cell)
print("\nImagesize: ",imagesize)
print("\n\n--------------------------------\n\n")


#################################################################################################################################

# ---------------------- HI CUBE ----------------------------------------------------------
direction = "J2000 15h00m00.0s -30d00m00.0s"
cl.done()
cl.addcomponent(dir=direction, flux=1.0, fluxunit='Jy', freq='1.420GHz', shape="Gaussian", majoraxis="0.1arcmin", minoraxis='0.05arcmin', positionangle='45.0deg')
ia.fromshape("HI_cube.im",[256,256,1,256],overwrite=True)
cs=ia.coordsys()
cs.setunits(['rad','rad','','Hz'])
cell_rad=qa.convert(qa.quantity(f"{del_theta_arcsec:0.01f}"),"rad")['value']
cs.setincrement([-cell_rad,cell_rad],'direction')
cs.setreferencevalue([qa.convert("15h",'rad')['value'],qa.convert("-30deg",'rad')['value']],type="direction")
cs.setreferencevalue(fcentre,'spectral')
cs.setincrement(del_nu,'spectral')
ia.setcoordsys(cs.torecord())
ia.setbrightnessunit("Jy/pixel")
ia.modify(cl.torecord(),subtract=False)
exportfits(imagename='HI_cube.im',fitsimage="HI_cube.fits",overwrite=True)

#---------------------------------------------------------------------------------------------------
